<?php
/**
 * Created by PhpStorm.
 * User: Marvin
 * Date: 5/24/16
 * Time: 16:55
 */

$dotenv = new \Dotenv\Dotenv(dirname(__DIR__) . '/');
$dotenv->overload();

$environment = getenv('ENVIRONMENT');

$environments = ['LOCAL', 'DEVELOP', 'STAGING', 'PRODUCTION'];

if (!in_array($environment, $environments)) {
    exit('Você precisa configurar o ENVIRONMENT no arquivo .env');
}

$envRequired[] = $environment . '_HOST';
$envRequired[] = $environment . '_USER';
$envRequired[] = $environment . '_PASSWORD';
$envRequired[] = $environment . '_DATABASE';
$envRequired[] = $environment . '_DEBUG';

$dotenv->required($envRequired);