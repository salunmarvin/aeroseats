<?php
/**
 * Created by PhpStorm.
 * User: Marvin
 * Date: 5/24/16
 * Time: 17:12
 */

namespace API\Entity;

/**
 * Interface EntityInterface
 * @package API\Entity
 */
interface EntityInterface
{
    public function __construct();

    public function getId();

    public function setId($id);

    public function getReferenceCode();

    public function setReferenceCode($referenceCode);

    public function generateReferenceCode();

    public function getCreatedAt();

    public function setCreatedAt($createdAt);

    public function getUpdatedAt();

    public function setUpdatedAt($updatedAt);

    public function getDeletedAt();

    public function setDeletedAt($deletedAt);
}