<?php

/**
 * Created by PhpStorm.
 * User: Marvin
 * Date: 5/24/16
 * Time: 17:15
 */

namespace API\Entity\Traits;

trait ReferenceCodeTrait
{
    private $referenceCode;

    /**
     * @return mixed
     */
    public function getReferenceCode()
    {
        return $this->referenceCode;
    }

    /**
     * @param mixed $referenceCode
     */
    public function setReferenceCode($referenceCode)
    {
        $this->referenceCode = $referenceCode;
    }

    public function generateReferenceCode()
    {
        $this->referenceCode = uniqid('', true);
    }
}