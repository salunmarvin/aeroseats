<?php

/**
 * Created by PhpStorm.
 * User: Marvin
 * Date: 5/24/16
 * Time: 17:51
 */

namespace API\Services\Responder;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class AbstractResponder
{
    public function verifyRequest(Request $request)
    {
        $data = new \stdClass();

        $data->content = json_decode($request->getContent());

        return $data;
    }

    public function createResponse($status, $message, $data = [], $duration = 3000)
    {
        $response = ['status' => $status, 'message' => $message, 'data' => $data, 'duration' => $duration];

        return new JsonResponse($response);
    }
}