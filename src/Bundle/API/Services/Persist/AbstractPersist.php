<?php
/**
 * Created by PhpStorm.
 * User: Marvin
 * Date: 5/26/16
 * Time: 11:47
 */

namespace API\Services\Persist;

use API\Entity\EntityInterface;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;

/**
 * Class AbstractPersistService
 * @package Common\Service\Persist
 */
abstract class AbstractPersist
{
    /**
     * @var EntityManager
     */
    protected $entityManager;

    /**
     * @param EntityManager $entityManager
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param EntityInterface $entity
     * @return EntityInterface
     * @throws ORMException
     * @throws \Exception
     */
    protected function create(EntityInterface $entity)
    {
        $entity->setCreatedAt(new \DateTime());

        if (is_null($entity->getReferenceCode())) {
            $entity->generateReferenceCode();
        }

        return $this->save($entity);
    }

    /**
     * @param EntityInterface $entity
     * @return EntityInterface
     * @throws ORMException
     * @throws \Exception
     */
    protected function delete(EntityInterface $entity)
    {
        $entity->setDeletedAt(new \DateTime());

        return $this->save($entity);
    }

    /**
     * @param EntityInterface $entity
     * @return EntityInterface
     * @throws ORMException
     * @throws \Exception
     */
    protected function update(EntityInterface $entity)
    {
        $entity->setUpdatedAt(new \DateTime());

        return $this->save($entity);
    }

    /**
     * @param EntityInterface $entity
     * @return EntityInterface
     * @throws ORMException
     * @throws \Exception
     */
    private function save(EntityInterface $entity)
    {
        try {
            $this->entityManager->persist($entity);
            $this->entityManager->flush();
        } catch (ORMException $e) {
            throw $e;
        }

        return $entity;
    }

    protected function remove(EntityInterface $entity)
    {
        try {
            $this->entityManager->remove($entity);
            $this->entityManager->flush();
        } catch (ORMException $e) {
            throw $e;
        }

        return $entity;
    }
}