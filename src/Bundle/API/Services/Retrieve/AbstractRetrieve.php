<?php

/**
 * Created by PhpStorm.
 * User: Marvin
 * Date: 5/24/16
 * Time: 18:02
 */

namespace API\Services\Retrieve;

use Doctrine\ORM\EntityRepository;

abstract class AbstractRetrieve
{
    /**
     * @var EntityRepository
     */
    protected $entityRepository;

    /**
     * @param EntityRepository $entityRepository
     */
    public function __construct(EntityRepository $entityRepository)
    {
        $this->entityRepository = $entityRepository;
    }

    abstract protected function getEntityRepository();

    /**
     * @return array
     */
    public function retrieveAll()
    {
        return $this->entityRepository->findBy(['deletedAt' => null]);
    }

    /**
     * @param $id
     * @return null|object
     */
    public function retrieveById($id)
    {
        return $this->entityRepository->findOneBy(['id' => $id]);
    }

    /**
     * @param $referenceCode
     * @param bool $isDeleted
     * @return null|object
     */
    public function retrieveByReferenceCode($referenceCode, $isDeleted = true)
    {
        $params = ['referenceCode' => $referenceCode];
        if ($isDeleted) {
            $params['deletedAt'] = null;
        }
        return $this->entityRepository->findOneBy($params);
    }
}