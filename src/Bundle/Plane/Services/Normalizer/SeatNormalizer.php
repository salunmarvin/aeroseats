<?php
/**
 * Created by PhpStorm.
 * User: Marvin
 * Date: 5/26/16
 * Time: 13:11
 */

namespace Plane\Services\Normalizer;

use API\Services\Normalizer\EntityNormalizer;

class SeatNormalizer extends EntityNormalizer
{
    public function referenceConfiguration()
    {
        $callbacks = [];
        $this->setCallbacks($callbacks);
    }

    public function ignoreAttributesConfiguration($fields = [])
    {
        parent::ignoreAttributesConfiguration(['plane']);
    }
}
