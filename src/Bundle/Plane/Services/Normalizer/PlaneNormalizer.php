<?php

/**
 * Created by PhpStorm.
 * User: Marvin
 * Date: 5/24/16
 * Time: 18:11
 */

namespace Plane\Services\Normalizer;

use API\Services\Normalizer\EntityNormalizer;

class PlaneNormalizer extends EntityNormalizer
{
    public function referenceConfiguration()
    {
        $callbacks = [];
        $this->setCallbacks($callbacks);
    }

    public function ignoreAttributesConfiguration($fields = [])
    {
        parent::ignoreAttributesConfiguration(['seats']);
    }
}