<?php
/**
 * Created by PhpStorm.
 * User: Marvin
 * Date: 5/26/16
 * Time: 13:01
 */

namespace Plane\Services\Retrieve;

use API\Services\Retrieve\AbstractRetrieve;
use Plane\Entity\Plane;

/**
 * Class SeatRetrieve
 * @package Plane\Services\Retrieve
 */
class SeatRetrieve extends AbstractRetrieve
{
    /**
     * @return \Plane\Entity\Repository\SeatRepository
     */
    protected function getEntityRepository()
    {
        return $this->entityRepository;
    }

    /**
     * @param Plane $plane
     * @return mixed
     */
    public function retrieveAllByPlane(Plane $plane)
    {
        return $this->getEntityRepository()->findAllByPlane($plane);
    }

    /**
     * @param Plane $plane
     * @param $initialLine
     * @return mixed
     */
    public function retrieveHalfByPlane(Plane $plane, $initialLine)
    {
        return $this->getEntityRepository()->findHalfByPlane($plane, $initialLine);
    }

    /**
     * @param Plane $plane
     * @return mixed
     */
    public function retrieveSeatsNumberOfLinesByPlane(Plane $plane)
    {
        return $this->getEntityRepository()->findSeatsNumberOfLinesByPlane($plane);
    }

    /**
     * @param Plane $plane
     * @return mixed
     */
    public function retrieveSeatsNumberOfColumnsByPlane(Plane $plane)
    {
        return $this->getEntityRepository()->findSeatsNumberOfColumnsByPlane($plane);
    }

    /**
     * @param Plane $plane
     * @param $fromLine
     * @param $toLine
     * @return int
     */
    public function retrieveNumberOfOccupiedSeatsOnSectorByPlane(Plane $plane, $fromLine, $toLine)
    {
        return $this->getEntityRepository()->findNumberOfOccupiedSeatsOnSectorByPlane($plane, $fromLine, $toLine);
    }
}
