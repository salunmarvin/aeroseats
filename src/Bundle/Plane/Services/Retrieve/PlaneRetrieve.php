<?php
/**
 * Created by PhpStorm.
 * User: Marvin
 * Date: 5/24/16
 * Time: 18:01
 */

namespace Plane\Services\Retrieve;

use API\Services\Retrieve\AbstractRetrieve;

/**
 * Class PlaneRetrieve
 * @package Plane\Services\Retrieve
 */
class PlaneRetrieve extends AbstractRetrieve
{
    protected function getEntityRepository()
    {
        return $this->entityRepository;
    }
}