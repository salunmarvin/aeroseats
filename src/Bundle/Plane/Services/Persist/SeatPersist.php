<?php
/**
 * Created by PhpStorm.
 * User: Marvin
 * Date: 5/26/16
 * Time: 13:05
 */

namespace Plane\Services\Persist;

use API\Services\Persist\AbstractPersist;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;
use Plane\Entity\Seat;
use Plane\Services\Retrieve\SeatRetrieve;

class SeatPersist extends AbstractPersist
{
    /**
     * @var SeatRetrieve
     */
    private $seatRetrieve;

    /**
     * SeatPersist constructor.
     * @param EntityManager $entityManager
     * @param SeatRetrieve $seatRetrieve
     */
    public function __construct(
        EntityManager $entityManager,
        SeatRetrieve $seatRetrieve
    ) {
        parent::__construct($entityManager);
        $this->seatRetrieve = $seatRetrieve;
    }

    public function process(Seat $seat)
    {
        /** @var Seat $seatFromDB */
        $seatFromDB = $this->seatRetrieve->retrieveByReferenceCode($seat->getReferenceCode());

        if (!is_null($seatFromDB)) {
            $seatFromDB->setColumn($seat->getColumn());
            $seatFromDB->setLine($seat->getLine());
            $seatFromDB->setPlane($seat->getPlane());

            return $this->update($seatFromDB);
        }

        return $this->create($seat);
    }

    public function processCollection(ArrayCollection $seats)
    {
        $seatsArray = [];

        foreach ($seats as $seat) {
            /** @var Seat $seatFromDB */
            $seatFromDB = $this->seatRetrieve->retrieveByReferenceCode($seat->getReferenceCode());

            $seatFromDB->setAvailable(0);

            $seatsArray[] = $seatFromDB->getSeatColumn().$seatFromDB->getLine();

            $this->update($seatFromDB);
        }

        return $seatsArray;
    }

    public function processDelete(Seat $seat)
    {
        return $this->delete($seat);
    }
}
