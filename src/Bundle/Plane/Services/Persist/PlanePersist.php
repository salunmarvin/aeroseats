<?php
/**
 * Created by PhpStorm.
 * User: Marvin
 * Date: 5/26/16
 * Time: 11:48
 */

namespace Plane\Services\Persist;

use API\Services\Persist\AbstractPersist;
use Doctrine\ORM\EntityManager;
use Plane\Entity\Plane;
use Plane\Services\Retrieve\PlaneRetrieve;

class PlanePersist extends AbstractPersist
{
    /**
     * @var PlaneRetrieve
     */
    private $planeRetrieve;

    /**
     * PlanePersist constructor.
     * @param EntityManager $entityManager
     * @param PlaneRetrieve $planeRetrieve
     */
    public function __construct(
        EntityManager $entityManager,
        PlaneRetrieve $planeRetrieve
    ) {
        parent::__construct($entityManager);
        $this->planeRetrieve = $planeRetrieve;
    }

    public function process(Plane $plane)
    {
        /** @var Plane $planeFromDB */
        $planeFromDB = $this->planeRetrieve->retrieveByReferenceCode($plane->getReferenceCode());

        if (!is_null($planeFromDB)) {
            $planeFromDB->setAircraftRegistration($plane->getAircraftRegistration());
            $planeFromDB->setModel($plane->getModel());

            if (!empty($plane->getSeats())) {
                $planeFromDB->setSeats($plane->getSeats());
            }

            return $this->update($planeFromDB);
        }

        return $this->create($plane);
    }

    public function processDelete(Plane $plane)
    {
        return $this->delete($plane);
    }
}