<?php
/**
 * Created by PhpStorm.
 * User: Marvin
 * Date: 5/24/16
 * Time: 17:47
 */

namespace Plane\Services\Responder;

use API\Services\Responder\AbstractResponder;
use Plane\Entity\Plane;
use Plane\Services\Normalizer\PlaneNormalizer;
use Plane\Services\Persist\PlanePersist;
use Plane\Services\Retrieve\PlaneRetrieve;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class PlaneResponder
 * @package Plane\Services\Responder
 */
class PlaneResponder extends AbstractResponder
{
    /**
     * @var PlaneRetrieve
     */
    private $planeRetrieve;

    /**
     * @var PlaneNormalizer
     */
    private $planeNormalizer;

    /**
     * @var PlanePersist
     */
    private $planePersist;

    /**
     * PlaneResponder constructor.
     * @param PlaneRetrieve $planeRetrieve
     * @param PlaneNormalizer $planeNormalizer
     * @param PlanePersist $planePersist
     */
    public function __construct(
        PlaneRetrieve $planeRetrieve,
        PlaneNormalizer $planeNormalizer,
        PlanePersist $planePersist
    ) {
        $this->planeRetrieve = $planeRetrieve;
        $this->planeNormalizer = $planeNormalizer;
        $this->planePersist = $planePersist;
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getAllPlanes(Request $request)
    {
        $planes = $this->planeRetrieve->retrieveAll();

        $normalizedPlanes = $this->planeNormalizer->normalizeCollection($planes);

        return $this->createResponse('success', 'Planes found.', $normalizedPlanes);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function putPlane(Request $request)
    {
        $data = $this->verifyRequest($request);

        /** @var Plane $plane */
        $plane = $this->planeNormalizer->denormalize($data->content, 'Plane\Entity\Plane');

        $planeNormalized = $this->planePersist->process($plane);

        return $this->createResponse('success', 'Added plane.', $planeNormalized);
    }
}