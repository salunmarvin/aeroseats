<?php
/**
 * Created by PhpStorm.
 * User: Marvin
 * Date: 5/26/16
 * Time: 13:12
 */

namespace Plane\Services\Responder;

use API\Services\Exception\InvalidArgumentHttpException;
use API\Services\Responder\AbstractResponder;
use Doctrine\Common\Collections\ArrayCollection;
use Plane\Entity\Plane;
use Plane\Entity\Seat;
use Plane\Services\Normalizer\SeatNormalizer;
use Plane\Services\Persist\SeatPersist;
use Plane\Services\Retrieve\PlaneRetrieve;
use Plane\Services\Retrieve\SeatRetrieve;
use Symfony\Component\HttpFoundation\Request;

class SeatResponder extends AbstractResponder
{
    /**
     * @var SeatRetrieve
     */
    private $seatRetrieve;

    /**
     * @var SeatNormalizer
     */
    private $seatNormalizer;

    /**
     * @var SeatPersist
     */
    private $seatPersist;

    /**
     * @var PlaneRetrieve
     */
    private $planeRetrieve;

    /**
     * SeatResponder constructor.
     * @param SeatRetrieve $seatRetrieve
     * @param SeatNormalizer $seatNormalizer
     * @param SeatPersist $seatPersist
     * @param PlaneRetrieve $planeRetrieve
     */
    public function __construct(
        SeatRetrieve $seatRetrieve,
        SeatNormalizer $seatNormalizer,
        SeatPersist $seatPersist,
        PlaneRetrieve $planeRetrieve
    ) {
        $this->seatRetrieve = $seatRetrieve;
        $this->seatNormalizer = $seatNormalizer;
        $this->seatPersist = $seatPersist;
        $this->planeRetrieve = $planeRetrieve;
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function getSeatsByPlane(Request $request, $planeReferenceCode)
    {
        /** @var Plane $plane */
        $plane = $this->planeRetrieve->retrieveByReferenceCode($planeReferenceCode);

        if (is_null($plane)) {
            throw new InvalidArgumentHttpException('Airplane does not exist');
        }

        $seats = $this->seatRetrieve->retrieveAllByPlane($plane);

        $normalizedSeats = $this->seatNormalizer->normalizeCollection($seats);

        return $this->createResponse('success', 'Seats found.', $normalizedSeats);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function putSeats(Request $request)
    {
        $data = $this->verifyRequest($request);

        /** @var Plane $plane */
        $plane = $this->planeRetrieve->retrieveByReferenceCode($data->content->plane);

        $seatsFinalArray = [];

        foreach ($data->content->seats as $seatFromArray) {
            /** @var Seat $seat */
            $seat = $this->seatNormalizer->denormalize($seatFromArray, 'Plane\Entity\Seat');

            $seat->setPlane($plane);

            $seatNormalized = $this->seatPersist->process($seat);

            $seatsFinalArray[] = $seatNormalized->getReferenceCode();
        }

        return $this->createResponse('success', 'Added seats.', $seatsFinalArray);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function generateSeats(Request $request)
    {
        $data = $this->verifyRequest($request);

        $columnsOrder = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'J', 'K'];

        $lines = $data->content->lines;
        $seatColumns = $data->content->seatColumns;

        if ($seatColumns > 10) {
            throw new InvalidArgumentHttpException('Maximum columns quantity: 10');
        }

        /** @var Plane $plane */
        $plane = $this->planeRetrieve->retrieveByReferenceCode($data->content->plane);

        for ($i = 1; $i <= $lines; $i++) {
            for ($j = 0; $j < $seatColumns; $j++) {
                $seat = [];
                $seat['seatColumn'] = $columnsOrder[$j];
                $seat['line'] = $i;
                $seat['available'] = 1;

                /** @var Seat $seat */
                $seat = $this->seatNormalizer->denormalize($seat, 'Plane\Entity\Seat');

                $seat->setPlane($plane);

                $this->seatPersist->process($seat);
            }
        }

        return $this->createResponse('success', 'Generated seats.', '');
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function bookSeats(Request $request)
    {
        $data = $this->verifyRequest($request);

        /** @var Plane $plane */
        $plane = $this->planeRetrieve->retrieveByReferenceCode($data->content->plane);

        $numberOfPassengers = $data->content->numberOfPassengers;
        $frontPreference = $data->content->frontPreference;

        $totalPlaneLines = $this->seatRetrieve->retrieveSeatsNumberOfLinesByPlane($plane);
        $totalPlaneSeatColumns = $this->seatRetrieve->retrieveSeatsNumberOfColumnsByPlane($plane);

        if ($numberOfPassengers < 1) {
            throw new InvalidArgumentHttpException('Minimum number of passengers: 1');
        }

        if ($numberOfPassengers > $totalPlaneSeatColumns) {
            throw new InvalidArgumentHttpException(
                "It's not possible to put together more than ".
                $totalPlaneSeatColumns
                ." passengers on this aircraft.");
        }

        $passengersOnFront = $this->seatRetrieve->retrieveNumberOfOccupiedSeatsOnSectorByPlane(
            $plane,
            1,
            ($totalPlaneLines/2)
        );

        $passengersOnBack = $this->seatRetrieve->retrieveNumberOfOccupiedSeatsOnSectorByPlane(
            $plane,
            ($totalPlaneLines/2) + 1,
            $totalPlaneLines
        );

        $loopStartNumber = 1;

        if (!$frontPreference) {
            $loopStartNumber = floor($totalPlaneLines/2) + 1;
        }

        if ($passengersOnBack < $passengersOnFront) {
            $loopStartNumber = floor($totalPlaneLines/2) + 1;
        }

        $actualLine = $loopStartNumber;

        $seats = $this->seatRetrieve->retrieveAllByPlane($plane);

        if ($loopStartNumber > 1) {
            $seats = $this->seatRetrieve->retrieveHalfByPlane($plane, count($seats)/2);
        }

        $seatsCollection = new ArrayCollection();

        /** @var Seat $seat */
        foreach ($seats as $seat) {
            if ($seat->getLine() != $actualLine) {
                $seatsCollection->clear();
            }

            if ($seat->getAvailable()) {
                $seatsCollection->add($seat);
            }
            if (!$seat->getAvailable()) {
                $seatsCollection->clear();
            }
            if (count($seatsCollection) == $numberOfPassengers) {
                break;
            }

            $actualLine = $seat->getLine();
        }

        $seatsReturn = $this->seatPersist->processCollection($seatsCollection);

        return $this->createResponse('success', 'Booked seats.', $seatsReturn);
    }
}
