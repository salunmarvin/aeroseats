<?php

/**
 * Created by PhpStorm.
 * User: Marvin
 * Date: 5/26/16
 * Time: 13:24
 */

namespace Plane\Entity\Repository;

use Doctrine\ORM\EntityRepository;
use Plane\Entity\Plane;

/**
 * Class SeatRepository
 * @package Plane\Entity\Repository
 */
class SeatRepository extends EntityRepository
{
    /**
     * @param Plane $plane
     * @return mixed
     */
    public function findAllByPlane(Plane $plane)
    {
        $qb = $this->createQueryBuilder('s');

        $qb
            ->innerJoin('s.plane', 'p')
            ->where('p.id = :plane_id')
            ->setParameter(':plane_id', $plane->getId());

        return $qb->getQuery()->execute();
    }

    /**
     * @param Plane $plane
     * @param $initialLine
     * @return mixed
     */
    public function findHalfByPlane(Plane $plane, $initialLine)
    {
        $qb = $this->createQueryBuilder('s');

        $qb
            ->innerJoin('s.plane', 'p')
            ->where('p.id = :plane_id')
            ->setFirstResult($initialLine)
            ->setParameter(':plane_id', $plane->getId());

        return $qb->getQuery()->execute();
    }

    /**
     * @param Plane $plane
     * @return int
     */
    public function findSeatsNumberOfLinesByPlane(Plane $plane)
    {
        $qb = $this->createQueryBuilder('s');

        $qb
            ->innerJoin('s.plane', 'p')
            ->where('p.id = :plane_id')
            ->groupBy('s.line')
            ->setParameter(':plane_id', $plane->getId());

        $result = $qb->getQuery()->execute();

        return count($result);
    }

    /**
     * @param Plane $plane
     * @return int
     */
    public function findSeatsNumberOfColumnsByPlane(Plane $plane)
    {
        $qb = $this->createQueryBuilder('s');

        $qb
            ->innerJoin('s.plane', 'p')
            ->where('p.id = :plane_id')
            ->groupBy('s.seatColumn')
            ->setParameter(':plane_id', $plane->getId());

        $result = $qb->getQuery()->execute();

        return count($result);
    }

    /**
     * @param Plane $plane
     * @param $fromLine
     * @param $toLine
     * @return int
     */
    public function findNumberOfOccupiedSeatsOnSectorByPlane(Plane $plane, $fromLine, $toLine)
    {
        $qb = $this->createQueryBuilder('s');

        $qb
            ->innerJoin('s.plane', 'p')
            ->where('p.id = :plane_id')
            ->andWhere('s.available = 0')
            ->andWhere('s.line >= :from_line')
            ->andWhere('s.line <= :to_line')
            ->groupBy('s.seatColumn')
            ->setParameter(':plane_id', $plane->getId())
            ->setParameter(':from_line', $fromLine)
            ->setParameter(':to_line', $toLine);

        $result = $qb->getQuery()->execute();

        return count($result);
    }
}
