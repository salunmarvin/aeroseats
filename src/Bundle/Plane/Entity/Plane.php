<?php
/**
 * Created by PhpStorm.
 * User: Marvin
 * Date: 5/24/16
 * Time: 17:04
 */

namespace Plane\Entity;

use API\Entity\EntityInterface;
use API\Entity\Traits;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class Plane
 * @package Plane\Entity
 */
class Plane implements EntityInterface
{
    use Traits\IdTrait;

    use Traits\ReferenceCodeTrait;

    use Traits\DateTrait;

    /**
     * @var String
     */
    private $model;

    /**
     * @var String
     */
    private $aircraftRegistration;

    /**
     * @var ArrayCollection
     */
    private $seats;

    public function __construct()
    {
        $this->generateReferenceCode();
        $this->createdAt = new \DateTime();
        $this->seats = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getModel()
    {
        return $this->model;
    }

    /**
     * @param mixed $model
     */
    public function setModel($model)
    {
        $this->model = $model;
    }

    /**
     * @return mixed
     */
    public function getAircraftRegistration()
    {
        return $this->aircraftRegistration;
    }

    /**
     * @param mixed $aircraftRegistration
     */
    public function setAircraftRegistration($aircraftRegistration)
    {
        $this->aircraftRegistration = $aircraftRegistration;
    }

    /**
     * @return ArrayCollection
     */
    public function getSeats()
    {
        return $this->seats;
    }

    /**
     * @param ArrayCollection $seats
     */
    public function setSeats($seats)
    {
        $this->seats = $seats;
    }

    /**
     * @param Seat $seat
     */
    public function addSeat(Seat $seat)
    {
        if (!$this->seats->contains($seat)) {
            $this->seats->add($seat);
        }
    }
}
