<?php
/**
 * Created by PhpStorm.
 * User: Marvin
 * Date: 5/24/16
 * Time: 17:04
 */

namespace Plane\Entity;

use API\Entity\EntityInterface;
use API\Entity\Traits;

class Seat implements EntityInterface
{
    use Traits\IdTrait;

    use Traits\ReferenceCodeTrait;

    use Traits\DateTrait;

    /**
     * @var String
     */
    private $seatColumn;

    /**
     * @var Integer
     */
    private $line;

    /**
     * @var Plane
     */
    private $plane;

    /**
     * @var Bool
     */
    private $available;

    public function __construct()
    {
        $this->generateReferenceCode();
        $this->createdAt = new \DateTime();
    }

    /**
     * @return mixed
     */
    public function getSeatColumn()
    {
        return $this->seatColumn;
    }

    /**
     * @param mixed $seatColumn
     */
    public function setSeatColumn($seatColumn)
    {
        $this->seatColumn = $seatColumn;
    }

    /**
     * @return mixed
     */
    public function getLine()
    {
        return $this->line;
    }

    /**
     * @param mixed $line
     */
    public function setLine($line)
    {
        $this->line = $line;
    }

    /**
     * @return mixed
     */
    public function getPlane()
    {
        return $this->plane;
    }

    /**
     * @param Plane $plane
     */
    public function setPlane(Plane $plane)
    {
        $this->plane = $plane;
    }

    /**
     * @return Bool
     */
    public function getAvailable()
    {
        return $this->available;
    }

    /**
     * @param Bool $available
     */
    public function setAvailable($available)
    {
        $this->available = $available;
    }
}
