<?php
/**
 * Created by PhpStorm.
 * User: Marvin
 * Date: 5/26/16
 * Time: 13:19
 */

$api->get(
    '/seats/plane/{planeReferenceCode}',
    function (\Symfony\Component\HttpFoundation\Request $request, $planeReferenceCode
    ) use ($api) {
        $seatResponder = $api[\Plane\Services\Responder\SeatResponder::class];

        return $seatResponder->getSeatsByPlane($request, $planeReferenceCode);
    });

$api->put('/seats', function (\Symfony\Component\HttpFoundation\Request $request) use ($api) {
    $seatResponder = $api[\Plane\Services\Responder\SeatResponder::class];

    return $seatResponder->putSeats($request);
});

$api->post('/seats/generate', function (\Symfony\Component\HttpFoundation\Request $request) use ($api) {
    $seatResponder = $api[\Plane\Services\Responder\SeatResponder::class];

    return $seatResponder->generateSeats($request);
});

$api->post('/seats/book', function (\Symfony\Component\HttpFoundation\Request $request) use ($api) {
    $seatResponder = $api[\Plane\Services\Responder\SeatResponder::class];

    return $seatResponder->bookSeats($request);
});
