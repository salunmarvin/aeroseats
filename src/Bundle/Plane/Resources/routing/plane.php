<?php
/**
 * Created by PhpStorm.
 * User: Marvin
 * Date: 5/24/16
 * Time: 18:00
 */

$api->get('/planes', function (\Symfony\Component\HttpFoundation\Request $request) use ($api) {
    $planeResponder = $api[\Plane\Services\Responder\PlaneResponder::class];

    return $planeResponder->getAllPlanes($request);
});

$api->put('/planes', function (\Symfony\Component\HttpFoundation\Request $request) use ($api) {
    $planeResponder = $api[\Plane\Services\Responder\PlaneResponder::class];

    return $planeResponder->putPlane($request);
});