<?php
/**
 * Created by PhpStorm.
 * User: Marvin
 * Date: 5/24/16
 * Time: 17:57
 */

$api[\Plane\Services\Responder\PlaneResponder::class] = function ($api) {
    return new \Plane\Services\Responder\PlaneResponder(
        $api[\Plane\Services\Retrieve\PlaneRetrieve::class],
        $api[\Plane\Services\Normalizer\PlaneNormalizer::class],
        $api[\Plane\Services\Persist\PlanePersist::class]
    );

};

$api[\Plane\Services\Retrieve\PlaneRetrieve::class] = function ($api) {
    /** @var \Doctrine\ORM\EntityManager $entityManager */
    $entityManager = $api[\Doctrine\ORM\EntityManager::class];

    return new \Plane\Services\Retrieve\PlaneRetrieve(
        $entityManager->getRepository(\Plane\Entity\Plane::class)
    );
};

$api[\Plane\Services\Persist\PlanePersist::class] = function ($api) {
    /** @var \Doctrine\ORM\EntityManager $entityManager */
    $entityManager = $api[\Doctrine\ORM\EntityManager::class];
    return new \Plane\Services\Persist\PlanePersist(
        $entityManager,
        $api[\Plane\Services\Retrieve\PlaneRetrieve::class]
    );
};

$api[\Plane\Services\Normalizer\PlaneNormalizer::class] = function () {
    $planeNormalizer = new \Plane\Services\Normalizer\PlaneNormalizer();
    $planeNormalizer->setSerializer(new \Symfony\Component\Serializer\Serializer([$planeNormalizer]));
    return $planeNormalizer;
};
