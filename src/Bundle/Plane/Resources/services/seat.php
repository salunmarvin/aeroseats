<?php
/**
 * Created by PhpStorm.
 * User: Marvin
 * Date: 5/26/16
 * Time: 13:11
 */

$api[\Plane\Services\Responder\SeatResponder::class] = function ($api) {
    return new \Plane\Services\Responder\SeatResponder(
        $api[\Plane\Services\Retrieve\SeatRetrieve::class],
        $api[\Plane\Services\Normalizer\SeatNormalizer::class],
        $api[\Plane\Services\Persist\SeatPersist::class],
        $api[\Plane\Services\Retrieve\PlaneRetrieve::class]
    );

};

$api[\Plane\Services\Retrieve\SeatRetrieve::class] = function ($api) {
    /** @var \Doctrine\ORM\EntityManager $entityManager */
    $entityManager = $api[\Doctrine\ORM\EntityManager::class];

    return new \Plane\Services\Retrieve\SeatRetrieve(
        $entityManager->getRepository(\Plane\Entity\Seat::class)
    );
};

$api[\Plane\Services\Persist\SeatPersist::class] = function ($api) {
    /** @var \Doctrine\ORM\EntityManager $entityManager */
    $entityManager = $api[\Doctrine\ORM\EntityManager::class];
    return new \Plane\Services\Persist\SeatPersist(
        $entityManager,
        $api[\Plane\Services\Retrieve\SeatRetrieve::class]
    );
};

$api[\Plane\Services\Normalizer\SeatNormalizer::class] = function () {
    $seatNormalizer = new \Plane\Services\Normalizer\SeatNormalizer();
    $seatNormalizer->setSerializer(new \Symfony\Component\Serializer\Serializer([$seatNormalizer]));
    return $seatNormalizer;
};
