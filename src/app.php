<?php

/** @var \Silex\Application $api */
$api = new Silex\Application();

require_once __DIR__ . '/env.php';

require_once __DIR__ . '/../config/database.php';

require_once __DIR__ . '/../src/Bundle/routing.php';
require_once __DIR__ . '/../src/Bundle/services.php';

return $api;
