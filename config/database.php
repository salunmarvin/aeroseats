<?php
/**
 * Created by PhpStorm.
 * User: Marvin
 * Date: 5/24/16
 * Time: 17:30
 */

use Doctrine\ORM\Configuration;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Mapping\Driver\SimplifiedYamlDriver;

$config = new Configuration();

$cache = new \Doctrine\Common\Cache\ApcCache();

$config->setProxyDir(__DIR__ . '/../data/Proxy');
$config->setProxyNamespace('EntityProxy');
$config->setAutoGenerateProxyClasses(\Doctrine\Common\Proxy\AbstractProxyFactory::AUTOGENERATE_FILE_NOT_EXISTS);

$driver = new SimplifiedYamlDriver([
    __DIR__ . '/../src/Bundle/Plane/Entity/Mapping' => 'Plane\Entity'
]);

$config->setMetadataDriverImpl($driver);

//getting the EntityManager
$entityManager = EntityManager::create(
    [
        'driver' => 'pdo_mysql',
        'host' => getenv(getenv('ENVIRONMENT') . '_HOST'),
        'port' => '3306',
        'user' => getenv(getenv('ENVIRONMENT') . '_USER'),
        'password' => getenv(getenv('ENVIRONMENT') . '_PASSWORD'),
        'dbname' => getenv(getenv('ENVIRONMENT') . '_DATABASE'),
        'charset' => 'utf8',
    ],
    $config
);

$api[\Doctrine\ORM\EntityManager::class] = $entityManager;

$api['debug'] = getenv(getenv('ENVIRONMENT') . '_DEBUG') === 'true';

if (getenv(getenv('ENVIRONMENT') . '_DEBUG') === 'true') {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(-1);
}